# Advanced Software Development
git submodule update --init lib/mopi
git submodule update --init lib/matlabdoc
git submodule update --init lib/ArgUtils
git submodule update --init lib/logging4matlab
git submodule update --init lib/MatlabProgressBar
git submodule update --init lib/matlab-ezparse
git submodule update --init lib/MATLAB-ParseArgs

### Pipeline, Job management
git submodule update --init lib/psom
git submodule update --init lib/matlab-batch
git submodule update --init lib/CachePureFunction
#git submodule update --init lib/ForEach    #Not a gitrepo
#git submodule update --init lib/DataHash
#git submodule update --init lib/MapN
#git submodule update --init lib/matlab_makefile

### Testing
git submodule update --init test/MOxUnit
git submodule update --init test/matlab-xunit

# Plotting Tools
git submodule update --init lib/BrewerMap
git submodule update --init lib/boundedline
git submodule update --init lib/legendflex
git submodule update --init lib/plotgrid
git submodule update --init lib/gramm
git submodule update --init lib/columnlegend
git submodule update --init lib/matlab-plotly


# Export Tools
git submodule update --init lib/latexTable
git submodule update --init lib/matlab-json
git submodule update --init lib/export_fig
git submodule update --init lib/saveFigure
git submodule update --init lib/matlab2tikz
git submodule update --init lib/jsonlab

# Convenience
git submodule update --init lib/MATLAB-git

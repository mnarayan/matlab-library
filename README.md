# matlab-library

This repository contains commonly used matlab packages, particularly those already open sourced. Where possible, packages have been added to the library using git submodules to add packages. 



## Installation

To clone this repository 

```bash
git clone git@github.com:TheEtkinLab/matlab-library.git matlab-library
```

To see the list of submodules and add a specific one 

```bash
cd matlab-library
git submodule status
git submodule update --init <name-of-submodule>
```

To install all the submodules 

```bash 
cd matlab-library
./install.sh
```

### Usage

**Add matlab-library into your project**

Suppose you have just created a new project in a directory called `my-project`. The directory contains the following subfolders 


	my-project
	    --- external
		--- src
		--- data
		--- results

Be sure to have an environment variable pointing to previously installed matlab-library using 

```bash
export MATLIBPATH=/to/path/matlab-library
``` 

Then copy libraries as needed into your project

``` bash 
cd my-project
cp -r $MATLIBPATH/lib .
cp -r $MATLIBPATH/test .
```

Your project will now contain two new subfolders.

	my-project
		--- lib
		--- src
		--- data
		--- results
		--- test

**Add matlab-library to your path**

Alternatively, you can simply add all the packages within matlab-library to your matlab path

```bash 
>> addpath(genpath('/path/to/matlab-library'))
```
or selectively add some packages as 

```bash
>> addpath(genpath('/path/to/matlab-library/lib/package-name'))
```

function exampleLegendreGaussQuad()

%%%%%%%%%%%%%%%%%%%%%% first example %%%%%%%%%%%%%%%%%%%%%%

arrl=0.06;
arrw=arrl/5;
arrScal=0.85;

spa=arrl/2;

fosi=20;
font='Times';

myLW=1.1;

pntsi=30;

xyticksL=0.025;

axls=[-1.25 1.25 -0.125  1.1875];

axls2=axls;
axls2(3)=-axls2(4)*0.55;
axvec=1.02*axls2;

clrPtch=[0.85,0.85,1];
clrPoly=[0,0,1];

sca=300;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

numfvals=600;
numPatchInt=120;

xstart=axls(1);
xend=axls(2);
xvals=linspace(xstart,xend,numfvals);
xPatchVals=[1 -1 linspace(-1,1,numPatchInt)];

myFunc=@(x)exp(1.1*x)/2.8;

intNumVal=integral(myFunc,-1,1,'RelTol',0,'AbsTol',1e-13);

hp=patch(xPatchVals,[0 0 myFunc(linspace(-1,1,numPatchInt))],'w');
set(hp,'FaceColor',clrPtch,'EdgeColor','none');


% Plot the coordinate axis
hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$x$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

xtmp=-1;
hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

xtmp=1;
hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');


hg0=plotInAxis(axls2,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hg=plotInAxis(axls2,xvals,myFunc(xvals),'k-','LineWidth',myLW);

xFuncText=0.5;
xInt=0.065;
hyfx=text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','k');

yInt1=-0.35;
text('Interpreter','latex','String','$\displaystyle \int_{-1}^1 f(x) \, dx$','Position',[-xInt,yInt1],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$=$','Position',[0,yInt1],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String',['$',num2str(intNumVal,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt1],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.0in';
includegraphicsOptions='';
figureDirectory='exLegendreGaussQuad1';
figcounter=0;
filename='exLegendreGaussQuad1';
frmps=1;

mkdir(figureDirectory);

axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

yInt2=-0.6;
text('Interpreter','latex','String','$=$','Position',[0,yInt2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);


for numNodes=1:8
    
    delete(hp)
    delete(hA1)
    delete(hA2)
    delete(hg0)
    delete(hg)
    delete(hyfx)
    delete(hxt1)
    delete(hxtlx1)
    delete(hxt2)
    delete(hxtlx2)
    
    [xNodes,wghs]=LegendreGaussNodesWeights(numNodes,-1,1);
    
    fval=myFunc(xNodes);
    lgI=sum(wghs.*fval);
    
    MyMat=zeros(numNodes,2*numNodes);
    for i=1:(2*numNodes-2)
        MyMat(:,i)=xNodes.^(2*numNodes-i);
    end
    MyMat(:,2*numNodes-1)=xNodes;
    MyMat(:,2*numNodes)=1;
    
    p0=zeros(2*numNodes,1);
    p0((numNodes+1):(2*numNodes))=MyMat(:,(numNodes+1):(2*numNodes))\fval;
    
    nullSpace=null(MyMat);
    myIntegrand=@(s,x)abs(myFunc(x)-polyval(nullSpace*s+p0,x));
    myDiffInt=@(s)integral(@(x)myIntegrand(s,x),-1,1);
    
    try
        q = fminunc(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    catch
        q = fminsearch(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    end
    
    p1=nullSpace*q+p0;
    
    hp=patch(xPatchVals,[0 0 polyval(p1,linspace(-1,1,numPatchInt))],'w');
    set(hp,'FaceColor',clrPtch,'EdgeColor','none');
    
    hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
    hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
    
    xtmp=-1;
    hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    xtmp=1;
    hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    hg0=plotInAxis(axls2,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hg=plotInAxis(axls2,xvals,myFunc(xvals),'k-','LineWidth',myLW);
    
    hpoly0=plotInAxis2(axls2,xvals,polyval(p1,xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hpoly=plotInAxis2(axls2,xvals,polyval(p1,xvals),'k-','LineWidth',myLW,'Color',clrPoly);
    
    hyfx=text('Interpreter','latex','String',['$y=P_{',num2str(2*numNodes-1),'}(x)$'],'Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    hlgnot=text('Interpreter','latex','String',['$\mathrm{LG}(f,[-1,1],',num2str(numNodes),')$'],'Position',[-xInt,yInt2],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    hlgval=text('Interpreter','latex','String',['$',num2str(lgI,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt2],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    xtmp=-1;
    hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    xtmp=1;
    hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    hxpticks0=zeros(1,numNodes);
    hxpticks=zeros(1,numNodes);
    for i=1:numNodes
        xtmp=xNodes(i);
        hxpticks0(i)=plot([xtmp,xtmp],[-1.1*xyticksL,0.1*xyticksL],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
        hxpticks(i)=plot([xtmp,xtmp],[-xyticksL,0],'k-','LineWidth',myLW);
    end
    
    hPoints=scatter(xNodes,fval,pntsi,'MarkerEdgeColor',[1 1 1]*0.99999,'MarkerFaceColor',clrPoly,'LineWidth',1.1*myLW);
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hpoly0)
    delete(hpoly)
    delete(hlgnot)
    delete(hlgval)
    delete(hxpticks0)
    delete(hxpticks)
    delete(hPoints)
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);

close;


%%%%%%%%%%%%%%%%%%%%%% Second example %%%%%%%%%%%%%%%%%%%%%%

arrl=0.06;
arrw=arrl/5;
arrScal=0.85;

spa=arrl/2;

fosi=20;
font='Times';

myLW=1.1;

pntsi=30;

xyticksL=0.025;

axls=[-1.25 1.25 -0.125  1.1875];

axls2=axls;
axls2(3)=-axls2(4)*0.55;
axvec=1.02*axls2;

clrPtch=[0.85,0.85,1];
clrPoly=[0,0,1];

sca=300;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

numfvals=600;
numPatchInt=120;

xstart=axls(1);
xend=axls(2);
xvals=linspace(xstart,xend,numfvals);
xPatchVals=[1 -1 linspace(-1,1,numPatchInt)];

myFunc=@(x)(4-x.^2-1.5*x).*cos(3*x)/7+0.05*x+0.35;

intNumVal=integral(myFunc,-1,1,'RelTol',0,'AbsTol',1e-13);

hp=patch(xPatchVals,[0 0 myFunc(linspace(-1,1,numPatchInt))],'w');
set(hp,'FaceColor',clrPtch,'EdgeColor','none');


% Plot the coordinate axis
hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$x$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

xtmp=-1;
hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

xtmp=1;
hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');


hg0=plotInAxis(axls2,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hg=plotInAxis(axls2,xvals,myFunc(xvals),'k-','LineWidth',myLW);

xFuncText=0.44;
xInt=0.065;
hyfx=text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','k');

yInt1=-0.35;
text('Interpreter','latex','String','$\displaystyle \int_{-1}^1 f(x) \, dx$','Position',[-xInt,yInt1],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$=$','Position',[0,yInt1],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String',['$',num2str(intNumVal,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt1],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.0in';
includegraphicsOptions='';
figureDirectory='exLegendreGaussQuad2';
figcounter=0;
filename='exLegendreGaussQuad2';
frmps=1;

mkdir(figureDirectory);

axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

yInt2=-0.6;
text('Interpreter','latex','String','$=$','Position',[0,yInt2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);


for numNodes=1:12
    
    delete(hp)
    delete(hA1)
    delete(hA2)
    delete(hg0)
    delete(hg)
    delete(hyfx)
    delete(hxt1)
    delete(hxtlx1)
    delete(hxt2)
    delete(hxtlx2)
    
    [xNodes,wghs]=LegendreGaussNodesWeights(numNodes,-1,1);
    
    fval=myFunc(xNodes);
    lgI=sum(wghs.*fval);
    
    MyMat=zeros(numNodes,2*numNodes);
    for i=1:(2*numNodes-2)
        MyMat(:,i)=xNodes.^(2*numNodes-i);
    end
    MyMat(:,2*numNodes-1)=xNodes;
    MyMat(:,2*numNodes)=1;
    
    p0=zeros(2*numNodes,1);
    p0((numNodes+1):(2*numNodes))=MyMat(:,(numNodes+1):(2*numNodes))\fval;
    
    nullSpace=null(MyMat);
    myIntegrand=@(s,x)abs(myFunc(x)-polyval(nullSpace*s+p0,x));
    myDiffInt=@(s)integral(@(x)myIntegrand(s,x),-1,1);
    
    try
        q = fminunc(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    catch
        q = fminsearch(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    end
    
    p1=nullSpace*q+p0;
    
    hp=patch(xPatchVals,[0 0 polyval(p1,linspace(-1,1,numPatchInt))],'w');
    set(hp,'FaceColor',clrPtch,'EdgeColor','none');
    
    hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
    hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
    
    xtmp=-1;
    hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    xtmp=1;
    hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    hg0=plotInAxis(axls2,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hg=plotInAxis(axls2,xvals,myFunc(xvals),'k-','LineWidth',myLW);
    
    hpoly0=plotInAxis2(axls2,xvals,polyval(p1,xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hpoly=plotInAxis2(axls2,xvals,polyval(p1,xvals),'k-','LineWidth',myLW,'Color',clrPoly);
    
    hyfx=text('Interpreter','latex','String',['$y=P_{',num2str(2*numNodes-1),'}(x)$'],'Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    hlgnot=text('Interpreter','latex','String',['$\mathrm{LG}(f,[-1,1],',num2str(numNodes),')$'],'Position',[-xInt,yInt2],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    hlgval=text('Interpreter','latex','String',['$',num2str(lgI,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt2],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    xtmp=-1;
    hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    xtmp=1;
    hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    hxpticks0=zeros(1,numNodes);
    hxpticks=zeros(1,numNodes);
    for i=1:numNodes
        xtmp=xNodes(i);
        hxpticks0(i)=plot([xtmp,xtmp],[-1.1*xyticksL,0.1*xyticksL],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
        hxpticks(i)=plot([xtmp,xtmp],[-xyticksL,0],'k-','LineWidth',myLW);
    end
    
    hPoints=scatter(xNodes,fval,pntsi,'MarkerEdgeColor',[1 1 1]*0.99999,'MarkerFaceColor',clrPoly,'LineWidth',1.1*myLW);
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hpoly0)
    delete(hpoly)
    delete(hlgnot)
    delete(hlgval)
    delete(hxpticks0)
    delete(hxpticks)
    delete(hPoints)
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);

close;


%%%%%%%%%%%%%%%%%%%%%% Third example %%%%%%%%%%%%%%%%%%%%%%

arrl=0.06;
arrw=arrl/5;
arrScal=0.85;

spa=arrl/2;

fosi=20;
font='Times';

myLW=1.1;

pntsi=30;

xyticksL=0.025;

axls=[-1.25 1.25 -0.125  1.1875];

axls2=axls;
axls2(3)=-axls2(4)*0.55;
axvec=1.02*axls2;

clrPtch=[0.85,0.85,1];
clrPoly=[0,0,1];

sca=300;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

numfvals=600;
numPatchInt=120;

xstart=axls(1);
xend=axls(2);
xvals=linspace(xstart,xend,numfvals);
xPatchVals=[1 -1 linspace(-1,1,numPatchInt)];

myFunc=@(x)abs(x);

intNumVal=integral(myFunc,-1,1,'RelTol',0,'AbsTol',1e-13);

hp=patch(xPatchVals,[0 0 myFunc(linspace(-1,1,numPatchInt))],'w');
set(hp,'FaceColor',clrPtch,'EdgeColor','none');


% Plot the coordinate axis
hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$x$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

xtmp=-1;
hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

xtmp=1;
hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');


hg0=plotInAxis(axls,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hg=plotInAxis(axls,xvals,myFunc(xvals),'k-','LineWidth',myLW);

xFuncText=0.5;
xInt=0.065;
hyfx=text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','k');

yInt1=-0.35;
text('Interpreter','latex','String','$\displaystyle \int_{-1}^1 f(x) \, dx$','Position',[-xInt,yInt1],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$=$','Position',[0,yInt1],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String',['$',num2str(intNumVal,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt1],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');



% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.0in';
includegraphicsOptions='';
figureDirectory='exLegendreGaussQuad3';
figcounter=0;
filename='exLegendreGaussQuad3';
frmps=1;

mkdir(figureDirectory);

axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

yInt2=-0.6;
text('Interpreter','latex','String','$=$','Position',[0,yInt2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);


for numNodes=1:20
    
    delete(hp)
    delete(hA1)
    delete(hA2)
    delete(hg0)
    delete(hg)
    delete(hyfx)
    delete(hxt1)
    delete(hxtlx1)
    delete(hxt2)
    delete(hxtlx2)
    
    [xNodes,wghs]=LegendreGaussNodesWeights(numNodes,-1,1);
    
    fval=myFunc(xNodes);
    lgI=sum(wghs.*fval);
    
    MyMat=zeros(numNodes,2*numNodes);
    for i=1:(2*numNodes-2)
        MyMat(:,i)=xNodes.^(2*numNodes-i);
    end
    MyMat(:,2*numNodes-1)=xNodes;
    MyMat(:,2*numNodes)=1;
    
    p0=zeros(2*numNodes,1);
    p0((numNodes+1):(2*numNodes))=MyMat(:,(numNodes+1):(2*numNodes))\fval;
    
    nullSpace=null(MyMat);
    myIntegrand=@(s,x)abs(myFunc(x)-polyval(nullSpace*s+p0,x));
    myDiffInt=@(s)integral(@(x)myIntegrand(s,x),-1,1);
    
    try
        q = fminunc(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    catch
        q = fminsearch(myDiffInt,zeros(numNodes,1),optimset('TolFun',0,'TolX',1e-14));
    end
    
    p1=nullSpace*q+p0;
    
    hp=patch(xPatchVals,[0 0 polyval(p1,linspace(-1,1,numPatchInt))],'w');
    set(hp,'FaceColor',clrPtch,'EdgeColor','none');
    
    hA1=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
    hA2=plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
    
    xtmp=-1;
    hxt1=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    xtmp=1;
    hxt2=plot([xtmp,xtmp],[-1.25*xyticksL,0],'k-','LineWidth',myLW);
    
    hg0=plotInAxis(axls,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hg=plotInAxis(axls,xvals,myFunc(xvals),'k-','LineWidth',myLW);
    
    if numNodes==1
        hpoly0=plot([xstart xend-1.4*arrl],[0 0],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
        hpoly=plot([xstart xend-1.3*arrl],[0 0],'k-','LineWidth',myLW,'Color',clrPoly);
    else
        hpoly0=plotInAxis2(axls,xvals,polyval(p1,xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
        hpoly=plotInAxis2(axls,xvals,polyval(p1,xvals),'k-','LineWidth',myLW,'Color',clrPoly);
    end
    
    hyfx=text('Interpreter','latex','String',['$y=P_{',num2str(2*numNodes-1),'}(x)$'],'Position',[xFuncText,myFunc(xFuncText)+spa],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    hlgnot=text('Interpreter','latex','String',['$\mathrm{LG}(f,[-1,1],',num2str(numNodes),')$'],'Position',[-xInt,yInt2],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    hlgval=text('Interpreter','latex','String',['$',num2str(lgI,'%1.10f\n'),'\ldots$'],'Position',[xInt,yInt2],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',clrPoly);
    
    xtmp=-1;
    hxtlx1(1)=text('Interpreter','latex','String','$-$','Position',[xtmp-0.55*spa,-spa-xyticksL],'HorizontalAlignment','right','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    hxtlx1(2)=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    xtmp=1;
    hxtlx2=text('Interpreter','latex','String','$1$','Position',[xtmp,-spa-xyticksL],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');
    
    hxpticks0=zeros(1,numNodes);
    hxpticks=zeros(1,numNodes);
    for i=1:numNodes
        xtmp=xNodes(i);
        hxpticks0(i)=plot([xtmp,xtmp],[-1.1*xyticksL,0.1*xyticksL],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
        hxpticks(i)=plot([xtmp,xtmp],[-xyticksL,0],'k-','LineWidth',myLW);
    end
    
    hPoints=scatter(xNodes,fval,pntsi,'MarkerEdgeColor',[1 1 1]*0.99999,'MarkerFaceColor',clrPoly,'LineWidth',1.1*myLW);
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hpoly0)
    delete(hpoly)
    delete(hlgnot)
    delete(hlgval)
    delete(hxpticks0)
    delete(hxpticks)
    delete(hPoints)
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);

close;


function h=plotInAxis(axls,xvals,yvals,varargin)

isInside=and(and(xvals>axls(1),xvals<axls(2)),and(yvals>axls(3),yvals<axls(4)));

h=plot(xvals(isInside),yvals(isInside),varargin{:});


function h=plotInAxis2(axls,xvals,yvals,varargin)

N=length(xvals);
N1=1;
Ne=N;
Nm=round(N/2);

for i=Nm:-1:1
    if not(and(and(xvals(i)>axls(1),xvals(i)<axls(2)),and(yvals(i)>axls(3),yvals(i)<axls(4))))
        N1=i+1;
        break
    end
end

for i=Nm:N
    if not(and(and(xvals(i)>axls(1),xvals(i)<axls(2)),and(yvals(i)>axls(3),yvals(i)<axls(4))))
        Ne=i-1;
        break
    end
end

h=plot(xvals(N1:Ne),yvals(N1:Ne),varargin{:});

function h=plotArrow(astart,aend,arrl,arrw,sc,lw,clr)

if nargin<7
    clr=[0,0,0];
end

h=[0,0];

dirc=aend-astart;

lgt=norm(dirc);

dirc=dirc/lgt;

xcords=lgt+[-sc*arrl -sc*arrl -arrl 0 -arrl -sc*arrl];
ycords=[0 arrw*1e-3 arrw 0 -arrw -arrw*1e-3];

aline=[astart astart+(lgt-0.5*sc*arrl)*dirc];

h(1)=plot(aline(1,:),aline(2,:),'-','LineWidth',lw,'Color',clr);

xcords2=dirc(1)*xcords-dirc(2)*ycords+astart(1);
ycords2=dirc(2)*xcords+dirc(1)*ycords+astart(2);

h(2)=patch(xcords2,ycords2,'k');

set(h(2),'FaceColor',clr,'EdgeColor',clr);


function [xNodes,wghs]=LegendreGaussNodesWeights(N,a,b)

N1=N;
N2=N+1;
N=N-1;

% Initial guess
tnodes=cos((2*(0:N)'+1)*pi/(2*N+2))+(0.27/N1)*sin(pi*linspace(-1,1,N1)'*N/N2);

% Legendre-Gauss Vandermonde Matrix
L=zeros(N1,N2);

tnOld=1e9*ones(N1,1);

% Solve the system
while max(abs(tnodes-tnOld))>eps
    
    L(:,1)=1;
    L(:,2)=tnodes;
    
    for j=2:N1
        L(:,j+1)=((2*j-1)*tnodes.*L(:,j)-(j-1)*L(:,j-1))/j;
    end
    
    Lp=(N2)*(L(:,N1)-tnodes.*L(:,N2))./(1-tnodes.^2);
    
    tnOld=tnodes;
    tnodes=tnOld-L(:,N2)./Lp;
    
    for j=1:floor(N1/2)
        tnodes(j)=(tnodes(j)-tnodes(N2-j))/2;
        tnodes(N2-j)=-tnodes(j);
    end
    
end

% Nodes on the interval [a,b] (scaling and translation)
xNodes=(a*(1-tnodes)+b*(1+tnodes))/2;

% Weights
wghs=(b-a)./((1-tnodes.^2).*Lp.^2)*(N2/N1)^2;

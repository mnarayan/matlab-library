function exampleCriterionFunction()

arrl=0.35;
arrw=arrl/5;
arrScal=0.85;

spa=0.15;
spa2=arrw+spa;

fosi=25;
font='Times';

myLW=1.1;

xyticksL=0.1;

axls=[-6 6 -2/(1+sqrt(5)) 22/(1+sqrt(5))];
axvec=1.02*axls;

sca=65;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$s$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


numfvals=200;

sstart=axls(1)+0.05;
send=axls(2)-0.05;

svals=linspace(sstart,send,numfvals);

kRob=2.5;
rhoFunc=4;

myFunc=@(s)criterionFun(s,kRob,rhoFunc);

plotInAxis(axls,svals,myFunc(svals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
plotInAxis(axls,svals,myFunc(svals),'k-','LineWidth',myLW);

xFuncText=2.5;
text('Interpreter','latex','String','$y=\varrho_{\hspace{0.04em}\mathrm{We}}(s)$','Position',[xFuncText,myFunc(xFuncText)],'HorizontalAlignment','left','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=3.5in';
includegraphicsOptions='';
figureDirectory='exCriterionFun';
figcounter=0;
filename='exCriterionFun';

mkdir(figureDirectory);

numApprx=25;

frmps=numApprx/3;

rvals=linspace(0,send-arrl,numApprx);

for i=1:numApprx
    
    etaVals = eta(svals,rvals(i),kRob,rhoFunc);
    
    heta0=plotInAxis(axls,svals,etaVals,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    heta=plotInAxis(axls,svals,etaVals,'b-','LineWidth',myLW);
    
    stext=3;
    hTtext=text('Interpreter','latex','String','$y=\eta(s,r)$','Position',[stext,eta(stext,rvals(i),kRob,rhoFunc)],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','b');
    
    hxt20=plot([rvals(i),rvals(i)],[-xyticksL*1.15,0],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hxt2=plot([rvals(i),rvals(i)],[-xyticksL,0],'k-','LineWidth',myLW);
    
    hp=patch([-1 1 1 -1]*0.15+rvals(i),[-1 -1 0 0]*0.31-xyticksL-0.1,'w');
    hp.FaceColor=[1 1 1]*0.99999;
    hp.EdgeColor='none';
    
    hxtl2=text('Interpreter','latex','String','$r$','Position',[rvals(i),-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

    delete(heta0);
    delete(heta);
    delete(hTtext);
    delete(hxt20);
    delete(hxt2);
    delete(hp);
    delete(hxtl2);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


function h=plotArrow(astart,aend,arrl,arrw,sc,lw,clr)

if nargin<7
    clr=[0,0,0];
end

h=[0,0];

dirc=aend-astart;

lgt=norm(dirc);

dirc=dirc/lgt;

xcords=lgt+[-sc*arrl -sc*arrl -arrl 0 -arrl -sc*arrl];
ycords=[0 arrw*1e-3 arrw 0 -arrw -arrw*1e-3];

aline=[astart astart+(lgt-0.5*sc*arrl)*dirc];

h(1)=plot(aline(1,:),aline(2,:),'-','LineWidth',lw,'Color',clr);

xcords2=dirc(1)*xcords-dirc(2)*ycords+astart(1);
ycords2=dirc(2)*xcords+dirc(1)*ycords+astart(2);

h(2)=patch(xcords2,ycords2,'k');

set(h(2),'FaceColor',clr,'EdgeColor',clr);


function vals = criterionFun(resid,kRob,rhoFunc)

N=length(resid);
vals=zeros(1,N);

switch rhoFunc
    case 1
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                vals(i) = -kRob*resid(i)-0.5*kRob^2;
            elseif resid(i) < kRob
                vals(i) = 0.5*resid(i)^2;
            else
                vals(i) = kRob*resid(i)-0.5*kRob^2;
            end
        end
    case 2
        % Tukey's bi-weight
        for i = 1:N
            if resid(i) < -kRob
                vals(i) = (kRob^2)/6;
            elseif resid(i) < kRob
                vals(i) = (kRob^2)/6*(1 - (1 - (resid(i)/kRob)^2)^3);
            else
                vals(i) = (kRob^2)/6;
            end
        end
    case 3
        % Cauchy
        vals = (kRob^2)/2*log(1 + (resid/kRob).^2);
    case 4
        % Welsch
        vals = (kRob^2)/2*(1-exp(-(resid/kRob).^2));
    otherwise
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                vals(i) = -kRob*resid(i)-0.5*kRob^2;
            elseif resid(i) < kRob
                vals(i) = 0.5*resid(i)^2;
            else
                vals(i) = kRob*resid(i)-0.5*kRob^2;
            end
        end
end


function wghs = getFirstDer(resid,kRob,rhoFunc)

N=length(resid);
wghs=zeros(1,N);

switch rhoFunc
    case 1
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = -kRob;
            elseif resid(i) < kRob
                wghs(i) = resid(i);
            else
                wghs(i) = kRob;
            end
        end
    case 2
        % Tukey's bi-weight
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = 0;
            elseif resid(i) < kRob
                wghs(i) = resid(i)*(1 - (resid(i)/kRob)^2)^2;
            else
                wghs(i) = 0;
            end
        end
    case 3
        % Cauchy
        wghs = resid./(1 + (resid/kRob).^2);
    case 4
        % Welsch
        wghs = resid.*exp(-(resid/kRob).^2);
    otherwise
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = -kRob;
            elseif resid(i) < kRob
                wghs(i) = resid(i);
            else
                wghs(i) = kRob;
            end
        end
end


function wghs = getSecondDer(resid,kRob,rhoFunc)

N=length(resid);
wghs=zeros(1,N);

switch rhoFunc
    case 1
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = 0;
            elseif resid(i) < kRob
                wghs(i) = 1;
            else
                wghs(i) = 0;
            end
        end
    case 2
        % Tukey's bi-weight
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = 0;
            elseif resid(i) < kRob
                wghs(i) = (1 - (resid(i)/kRob)^2)*(1-5*(resid(i)/kRob)^2);
            else
                wghs(i) = 0;
            end
        end
    case 3
        % Cauchy
        wghs = (1 - (resid/kRob).^2)./(((1 + (resid/kRob).^2)).^2);
    case 4
        % Welsch
        wghs = (1 - 2*(resid/kRob).^2).*exp(-(resid/kRob).^2);
    otherwise
        % Huber
        for i = 1:N
            if resid(i) < -kRob
                wghs(i) = 0;
            elseif resid(i) < kRob
                wghs(i) = 1;
            else
                wghs(i) = 0;
            end
        end
end


function vals = eta(svals,r,kRob,rhoFunc)

if abs(r)<1e-6
    vals = 0.5*getSecondDer(0,kRob,rhoFunc)*svals.^2;
else
    vals = criterionFun(r,kRob,rhoFunc) - 0.5*getFirstDer(r,kRob,rhoFunc)*r + (0.5*getFirstDer(r,kRob,rhoFunc)/r)*svals.^2;
end


function h=plotInAxis(axls,xvals,yvals,varargin)

isInside=and(and(xvals>axls(1),xvals<axls(2)),and(yvals>axls(3),yvals<axls(4)));

h=plot(xvals(isInside),yvals(isInside),varargin{:});

\documentclass[12pt]{article}

\title{Animate\LaTeX{} for \textsc{Matlab}\\{\normalsize (Open the document using Adobe Acrobat's pdf-reader)}}			
\author{Per Bergstr\"{o}m}		
\date{December 2016}					

\usepackage{animate}
\usepackage{graphicx}           

\begin{document}
\maketitle

\section{Tangent Line}

$f'(x_{0})=\displaystyle \lim_{h \rightarrow 0} \frac{f(x_{0}+h)-f(x_{0})}{h}$

\begin{center}
\input{exTangLine/exTangLine}
\end{center}

\newpage

\section{Taylor Polynomial}

(Maclaurin polynomial)

\noindent
\vspace{2.5em}$P_n(x)=f(0)+f'(0)x+\displaystyle\frac{f''(0)}{2}x^2+\ldots+\displaystyle\frac{f^{(n)}(0)}{n!}x^n$


\begin{center}
\input{exTaylorPoly/exTaylorPoly}
\end{center}

\begin{center}
\input{exTaylorPoly2/exTaylorPoly2}
\end{center}

\newpage

\section{Fourier Series}

\begin{center}
\input{exFourierSerie1/exFourierSerie1}
\end{center}
\vspace{1.5em}
\begin{center}
\input{exFourierSerie2/exFourierSerie2}
\end{center}
\vspace{1.5em}
\begin{center}
\input{exFourierSerie3/exFourierSerie3}
\end{center}

\newpage

\section{Solution to PDE}
Let $f$ be a $2\pi$-periodic function
\[
f(\theta) = \left\{
\begin{array}{lr@{\,\,}c@{\,\,}c@{\,\,}c@{\,\,}l}
0, & -\pi & \leq & \theta & < & -\pi/2 \\
220, & -\pi/2 & \leq & \theta & < & \pi/2 \\
0, & \pi/2 & \leq & \theta & < & \pi
\end{array}
\right.
\]
PDE
\[
\left\{
\begin{array}{lll}
\frac{\partial ^2 u(r,\theta)}{\partial r ^2}+\frac{1}{r}\frac{\partial u(r,\theta)}{\partial r}+\frac{1}{r^2}\frac{\partial^2 u(r,\theta)}{\partial \theta ^2} = 0, & 0 < r < 1,  &  0 < \theta \leq 2\pi \\
u(1,\theta)=f(\theta) &  &  
\end{array}
\right.
\]
\vspace{1em}


\begin{center}
\input{exFourierSerie4/exFourierSerie4}
\end{center}


\begin{center}
\input{exPDE1/exPDE1}
\end{center}


\newpage

\section{The Newton-Raphson Method}
$f(x)=0$

\noindent
\vspace{2.5em}$x_{k+1}=x_{k}-\displaystyle\frac{f(x_{k})}{f'(x_{k})},\;k=0,1,\ldots$

\begin{center}
\input{exNewton/exNewton}
\end{center}

\begin{center}
\input{exNewton2/exNewton2}
\end{center}

\newpage

\section{The Bisection Method}

\begin{center}
\input{exBisection/exBisection}
\end{center}

\section{Least Squares}
$\hat{\mathbf{x}} = \displaystyle \arg \min_{\mathbf{x}} || \mathbf{A}\mathbf{x}-\mathbf{b} ||_2^2$

\begin{center}
\input{exLS/exLS}
\end{center}						

\newpage

\section{Curvature}

\begin{center}
\input{exCurvature/exCurvature}
\end{center}

\section{Solid of Revolution}

\begin{center}
\input{exSolidRev/exSolidRev}

(Press the play button again after the pauses)
\end{center}

\newpage

\section{Riemann Sums}

$U(f,P)=\displaystyle \sum_{i=1}^n f(l_i)\Delta x_i$\newline
$L(f,P)=\displaystyle \sum_{i=1}^n f(u_i)\Delta x_i$

\begin{center}
\input{exRiemann/exRiemann}
\end{center}

\newpage


\section{The Trapezoidal Rule}
The integral $\int_a^b f(x)dx$ can be approximated by
\[
T(f,P_n)=\displaystyle\sum_{i=1}^{n}\frac{f(x_{i-1})+f(x_{i})}{2}\Delta x_i  \, .
\]
If the value of $\Delta x_i$ is the same for $i=1,2,\ldots,n$, i.e. $\Delta x_i=h=(b-a)/n$, the expression of $T(f,P_n)$ can be simplified as
\[
T(f,P_n)=h\displaystyle\sum_{i=1}^{n-1}f(x_{i})  +   h\frac{f(x_{0})+f(x_{n})}{2} \, .
\]
\begin{center}
\input{exTrapezoidalRule/exTrapezoidalRule}
\end{center}



\newpage

\section{Legendre-Gauss Quadrature}

In numerical analysis, a quadrature rule is an approximation of the definite integral of a function $f$, usually stated as a weighted sum of function values at specified points within the domain of integration. An $n$-point Legendre-Gauss quadrature rule is a quadrature rule constructed to yield an exact result for polynomials $P$ of degree less or equal to $2n - 1$ by a suitable choice of the points $x_i$ and weights $w_i$ for $i = 1, \ldots, n$. The domain of integration for such a rule is conventionally taken as $[-1, 1]$, so the rule is stated as
\[
\displaystyle\int_{-1}^1 P(x) dx = \displaystyle\sum_{i=1}^n w_i P(x_i) \, .
\]
Legendre-Gauss quadrature as above will only produce good results if the function $f$ is well approximated by a polynomial function within the range $[-1, 1]$. The method is not, for example, suitable for functions with singularities. An integral of a function $f$ is approximated using Legendre-Gauss quadrature as
\[
\displaystyle\int_{-1}^1 f(x) dx \approx \displaystyle\sum_{i=1}^n w_i f(x_i) \, .
\]
\vspace{1em}
\begin{center}
\input{exLegendreGaussQuad1/exLegendreGaussQuad1}
\end{center}

\newpage

\begin{center}
\input{exLegendreGaussQuad2/exLegendreGaussQuad2}
\end{center}
\vspace{4em}
\begin{center}
\input{exLegendreGaussQuad3/exLegendreGaussQuad3}
\end{center}

\newpage

\section{Robust Criterion Functions}
Robust criterion functions are used for estimating parameters under a robust criterion. That is to avoid a large influence of outliers. If least-square minimization is used, strongly deviating data will have a huge influence of the estimate.\\[2em]
Welsch's criterion function:
\[
\varrho_{\hspace{0.04em}\mathrm{We}}(r)=\frac{\kappa_{\hspace{0.04em}\mathrm{We}}^2}{2} \left( 1-\hspace{0.02em}\exp \hspace{-0.02em}\left(-\frac{r^2}{\kappa_{\hspace{0.04em}\mathrm{We}}^2}\right) \right)
\]
\\[1em]
Huber's  criterion function:
\[
\varrho_{\hspace{0.04em}\mathrm{Hu}}(r)=\left\{ \begin{array}{ll}
         \frac{1}{2}r^2 & \mbox{if $|r| \leq \kappa_{\hspace{0.04em}\mathrm{Hu}}$}\\
        \kappa_{\hspace{0.04em}\mathrm{Hu}}|r|-\frac{\kappa_{\hspace{0.04em}\mathrm{Hu}}^2}{2} & \mbox{if $|r| > \kappa_{\hspace{0.04em}\mathrm{Hu}}$}\end{array} \right. 
\]
\\[1em]
Approximation:
\[
\eta(s,r)=\left\{ \begin{array}{@{\,}l@{\hspace{1.5em}}l@{\,}}
\vspace{0.4em}
         \varrho(r)-\frac{\varrho'(r)}{2}r+\frac{\varrho'(r)}{2r}s^2 & \mbox{if $r \neq 0$}\\
         \frac{\varrho''(0)}{2}s^2 & \mbox{if $r = 0$}\end{array} \right.
\]

\vspace{2em}
\noindent{\bf Reference}\\[0.7em]
Bergstr\"{o}m, P. and Edlund, O. 2014, ``Robust registration of point sets using iteratively reweighted least squares'', Computational Optimization and Applications, vol 58, no. 3, pp. 543-561, doi: 10.1007/s10589-014-9643-2
\newpage

\begin{center}
{\large Welsch's  criterion function $\varrho_{\hspace{0.04em}\mathrm{We}}$ and its approximation $\eta$}\\[1.5em]
\input{exCriterionFun/exCriterionFun}
\end{center}
\vspace{4em}
\begin{center}
{\large Huber's criterion function $\varrho_{\hspace{0.04em}\mathrm{Hu}}$ and its approximation $\eta$}\\[1.5em]
\input{exCriterionFun2/exCriterionFun2}
\end{center}

\newpage


\section{Sine and Cosine}
Moving sine and cosine function
\begin{center}
\input{example1/example1}
\end{center}
The animation is created using \texttt{animategraphicsLaTeX}.

\section{Rotating Points}
Points on a sphere rotating around its center
\begin{center}
\input{example2/example2}
\end{center}
The animation is created using \texttt{animateinlineLaTeX}.

\newpage

\section{Animations}
Lots of different animations can easily be created using \texttt{animateLaTeX} in \textsc{Matlab}. This might for example be appropriate in pdf-presentations. The command \texttt{$\backslash$animategraphics} is used in \texttt{animategraphicsLaTeX} and the environment \texttt{'animateinline'} is used in \texttt{animateinlineLaTeX}. Both \texttt{$\backslash$animategraphics} and \texttt{'animateinline'} are in the \LaTeX{} \texttt{animate} Package\footnote{http://ctan.uib.no/macros/latex/contrib/animate/animate.pdf}. In \texttt{animateinlineLaTeX} each frame might have different settings, such as trimming, size etc. \texttt{$\backslash$includegraphics} is used for inserting graphics into the frames. The settings are inserted into the optional command inside the brackets. See documentation for \texttt{$\backslash$includegraphics} for more infromation.

\end{document}


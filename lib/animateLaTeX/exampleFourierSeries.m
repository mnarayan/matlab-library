function exampleFourierSeries()

arrl=0.35;
arrw=arrl/5;
arrScal=0.85;

spa=0.15;

fosi=25;
font='Times';

myLW=1.1;

axls=[-10.5 10.5 -4.35 4.35];
axvec=1.02*axls;

sca=50;

masi0=18;
masi1=25;

sstart=axls(1)+0.1;
send=axls(2)-0.1;

numfvals=1200;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$t$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


a=0.53;
b=1-a;
kkk=1.89;

myZeroFun=@(t)a*t+kkk*b*sin(t/kkk)-sstart;

tmpStart=fzero(myZeroFun,sstart);

myZeroFun=@(t)a*t+kkk*b*sin(t/kkk)-send;

tmpEnd=fzero(myZeroFun,send);

svals=a*linspace(tmpStart,tmpEnd,numfvals)+kkk*b*sin(linspace(tmpStart,tmpEnd,numfvals)/kkk);


i=-2;
plot([sstart pi+i*2*pi],[sstart-i*2*pi pi],'k-','LineWidth',myLW)
plot(pi+i*2*pi,pi,'k.','MarkerSize',masi1)
plot(pi+i*2*pi,pi,'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)

plot(-3*pi,0,'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
plot(-3*pi,0,'k.','MarkerSize',masi0)

for i=-1:1
    plot([-pi pi]+i*2*pi,[-pi pi],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999)
    plot([-pi pi]+i*2*pi,[-pi pi],'k.-','LineWidth',myLW,'MarkerSize',masi1)
    plot([-pi pi]+i*2*pi,[-pi pi],'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)
    plot(pi+i*2*pi,0,'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
    plot(pi+i*2*pi,0,'k.','MarkerSize',masi0)
end

i=2;
plot([-pi+i*2*pi send],[-pi send-i*2*pi],'k-','LineWidth',myLW)
plot(-pi+i*2*pi,-pi,'k.','MarkerSize',masi1)
plot(-pi+i*2*pi,-pi,'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.5in';
includegraphicsOptions='';
figureDirectory='exFourierSerie1';
figcounter=0;
filename='exFourierSerie1';

mkdir(figureDirectory);

numTerms=30;

frmps=numTerms/6;

sN=zeros(1,numfvals);
theSign=1;

for i=1:numTerms
    
    sN=sN+(2*theSign/i)*sin(i*svals);
    theSign=-theSign;
    
    hsN0=plotInAxis(axls,svals,sN,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hsN=plotInAxis(axls,svals,sN,'b-','LineWidth',myLW);
    
    hTtext=text('Interpreter','latex','String',['$y=s_{',num2str(i),'}(t)$'],'Position',[3.4,pi/2],'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','b');
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hsN0);
    delete(hsN);
    delete(hTtext);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$t$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


a=0.53;
b=1-a;
kkk=1.89/2;
ddd=2.95;

myZeroFun=@(t)a*t+kkk*b*sin((t-ddd)/kkk)-sstart;

tmpStart=fzero(myZeroFun,sstart);

myZeroFun=@(t)a*t+kkk*b*sin((t-ddd)/kkk)-send;

tmpEnd=fzero(myZeroFun,send);

svals=a*linspace(tmpStart,tmpEnd,numfvals)+kkk*b*sin((linspace(tmpStart,tmpEnd,numfvals)-ddd)/kkk);


i=-2;
plot([sstart pi+i*2*pi],[pi pi],'k-','LineWidth',myLW)
plot(pi+i*2*pi,pi,'k.','MarkerSize',masi1)
plot(pi+i*2*pi,pi,'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)

plot(-3*pi,0,'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
plot(-3*pi,0,'k.','MarkerSize',masi0)

plot([0 0],[-pi pi],'.','MarkerSize',1.23*masi1,'Color',[1 1 1]*0.99999)

for i=-1:1
    plot([-pi 0]+i*2*pi,[-pi -pi],'k.-','LineWidth',myLW,'MarkerSize',masi1)
    plot([0 pi]+i*2*pi,[pi pi],'k.-','LineWidth',myLW,'MarkerSize',masi1)
    
    plot([-pi 0 0 pi]+i*2*pi,[-pi -pi pi pi],'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)
    
    plot(i*2*pi,0,'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
    plot(i*2*pi,0,'k.','MarkerSize',masi0)
    
    plot(pi+i*2*pi,0,'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
    plot(pi+i*2*pi,0,'k.','MarkerSize',masi0)
end

i=2;
plot([-pi+i*2*pi send],[-pi -pi],'k-','LineWidth',myLW)
plot(-pi+i*2*pi,-pi,'k.','MarkerSize',masi1)
plot(-pi+i*2*pi,-pi,'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.5in';
includegraphicsOptions='';
figureDirectory='exFourierSerie2';
figcounter=0;
filename='exFourierSerie2';

mkdir(figureDirectory);

numTerms=30;

frmps=numTerms/6;

sN=zeros(1,numfvals);

for i=1:numTerms
    
    sN=sN+(4/(2*i-1))*sin((2*i-1)*svals);
    
    hsN0=plotInAxis(axls,svals,sN,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hsN=plotInAxis(axls,svals,sN,'b-','LineWidth',myLW);
    
    hTtext=text('Interpreter','latex','String',['$y=s_{',num2str(2*i-1),'}(t)$'],'Position',[3*pi/2,pi/2],'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','b');
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hsN0);
    delete(hsN);
    delete(hTtext);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$t$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


svals=linspace(sstart,send,numfvals);

for i=-1:1
    plot([-pi 0 pi]+i*2*pi,[-pi pi -pi],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999)
end

i=-2;
plot([sstart pi+i*2*pi],[(pi+i*2*pi-sstart)*2-pi -pi],'k-','LineWidth',myLW)

for i=-1:1
    plot([-pi 0 pi]+i*2*pi,[-pi pi -pi],'k-','LineWidth',myLW)
end

i=2;
plot([-pi+i*2*pi send],[-pi (send-(-pi+i*2*pi))*2-pi],'k-','LineWidth',myLW)


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.5in';
includegraphicsOptions='';
figureDirectory='exFourierSerie3';
figcounter=0;
filename='exFourierSerie3';

mkdir(figureDirectory);

numTerms=25;

frmps=numTerms/6;

sN=zeros(1,numfvals);

for i=1:numTerms
    
    sN=sN+(8/(pi*(2*i-1)^2))*cos((2*i-1)*svals);
    
    hsN0=plotInAxis(axls,svals,sN,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hsN=plotInAxis(axls,svals,sN,'b-','LineWidth',myLW);
    
    hTtext=text('Interpreter','latex','String',['$y=s_{',num2str(2*i-1),'}(t)$'],'Position',[pi,pi/2],'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','b');
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hsN0);
    delete(hsN);
    delete(hTtext);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


axls=[-10.5 10.5 -4.35/5 4.35];
axvec=1.02*axls;

send=axls(2)-0.4;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$\theta$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

htick=0.1;

i=-3;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$-3\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

i=-2;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$-2\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

i=-1;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$-\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

i=1;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

i=2;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$2\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');

i=3;
plot([i*pi i*pi],[-htick 0],'k-','LineWidth',myLW)
text('Interpreter','latex','String','$3\pi$','Position',[i*pi,-1.1*htick],'HorizontalAlignment','center','VerticalAlignment','top','FontSize',fosi,'FontName',font,'Color','k');


a=0.53;
b=1-a;
kkk=1.89/2;
ddd=2.95;

myZeroFun=@(t)a*t+kkk*b*cos((t-ddd)/kkk)-sstart;

tmpStart=fzero(myZeroFun,sstart);

myZeroFun=@(t)a*t+kkk*b*cos((t-ddd)/kkk)-send;

tmpEnd=fzero(myZeroFun,send);

svals=a*linspace(tmpStart,tmpEnd,numfvals)+kkk*b*cos((linspace(tmpStart,tmpEnd,numfvals)-ddd)/kkk);


i=-2;
plot([sstart pi+i*2*pi],[0 0],'k-','LineWidth',1.1*myLW)

for i=-1:1
    plot([-pi -pi/2]+i*2*pi,[0 0],'k-','LineWidth',1.1*myLW)
    plot([-pi/2 pi/2]+i*2*pi,[pi pi],'k-','LineWidth',1.1*myLW)
    plot([pi/2 pi]+i*2*pi,[0 0],'k-','LineWidth',1.1*myLW)
    plot([pi/2 pi]+i*2*pi,[0 0],'k-','LineWidth',1.1*myLW)
    
    plot(i*2*pi-pi/2,0,'.','MarkerSize',1.23*masi1,'Color',[1 1 1]*0.99999)
    plot([-pi/2 pi/2]+i*2*pi,[0,pi],'k.','MarkerSize',masi1)
    plot([-pi/2 pi/2]+i*2*pi,[0,pi],'.','MarkerSize',masi0,'Color',[1 1 1]*0.99999)
    
    plot([-pi/2 pi/2]+i*2*pi,[pi,0],'.','MarkerSize',1.33*masi0,'Color',[1 1 1]*0.99999)
    plot([-pi/2 pi/2]+i*2*pi,[pi,0],'k.','MarkerSize',masi0)
end

i=2;
plot([-pi+i*2*pi send],[0 0],'k-','LineWidth',1.1*myLW)

text('Interpreter','latex','String','$y=f(\theta)$','Position',[-pi,pi/4],'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=4.5in';
includegraphicsOptions='';
figureDirectory='exFourierSerie4';
figcounter=0;
filename='exFourierSerie4';

mkdir(figureDirectory);

numTerms=30;

frmps=numTerms/6;

sN=(pi/2)*ones(1,numfvals);


axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);


hsN0=plotInAxis(axls,svals,sN,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hsN=plotInAxis(axls,svals,sN,'b-','LineWidth',myLW);

hTtext=text('Interpreter','latex','String','$y=s_{0}(\theta)$','Position',[pi,3*pi/4],'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','b');

axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

delete(hsN0);
delete(hsN);
delete(hTtext);

n=1;
coefPositive=true;

for i=2:numTerms
    
    if coefPositive
        sN=sN+(2/n)*cos(n*svals);
    else
        sN=sN-(2/n)*cos(n*svals);
    end
    
    hsN0=plotInAxis(axls,svals,sN,'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hsN=plotInAxis(axls,svals,sN,'b-','LineWidth',myLW);
    
    hTtext=text('Interpreter','latex','String',['$y=s_{',num2str(n),'}(\theta)$'],'Position',[pi,3*pi/4],'HorizontalAlignment','center','VerticalAlignment','middle','FontSize',fosi,'FontName',font,'Color','b');
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hsN0);
    delete(hsN);
    delete(hTtext);
    
    n=n+2;
    coefPositive=not(coefPositive);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


function h=plotArrow(astart,aend,arrl,arrw,sc,lw,clr)

if nargin<7
    clr=[0,0,0];
end

h=[0,0];

dirc=aend-astart;

lgt=norm(dirc);

dirc=dirc/lgt;

xcords=lgt+[-sc*arrl -sc*arrl -arrl 0 -arrl -sc*arrl];
ycords=[0 arrw*1e-3 arrw 0 -arrw -arrw*1e-3];

aline=[astart astart+(lgt-0.5*sc*arrl)*dirc];

h(1)=plot(aline(1,:),aline(2,:),'-','LineWidth',lw,'Color',clr);

xcords2=dirc(1)*xcords-dirc(2)*ycords+astart(1);
ycords2=dirc(2)*xcords+dirc(1)*ycords+astart(2);

h(2)=patch(xcords2,ycords2,'k');

set(h(2),'FaceColor',clr,'EdgeColor',clr);


function h=plotInAxis(axls,xvals,yvals,varargin)

isInside=and(and(xvals>axls(1),xvals<axls(2)),and(yvals>axls(3),yvals<axls(4)));

h=plot(xvals(isInside),yvals(isInside),varargin{:});


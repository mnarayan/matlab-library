function examplesPDE()

liwi=1;
fosi=20;
font='Times';

printResolution='-r110';

lenArr=0.075;
radi=lenArr/5;

spa=0.1;


viewVec=[-1.5 -2 1];

axvec=[-1.25 1.25 -1.25  1.25 -0.25 1.25];


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=3.5in';
includegraphicsOptions='';
figureDirectory='exPDE1';
figcounter=0;
filename='exPDE1';
frmps=5;

mkdir(figureDirectory);


figure(1),hold on
set(gcf,'Position',[50 50 750 650])


axis equal
set(gca,'XTick',[])
set(gca,'XTickLabel',{})
set(gca,'YTick',[])
set(gca,'YTickLabel',{})
set(gca,'ZTick',[])
set(gca,'ZTickLabel',{})

set(gca,'Position',[-0.3 -0.25 1.6 1.5])
axis off

view(viewVec)

plot3(axvec(1),axvec(3),axvec(5),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(2),axvec(3),axvec(5),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(1),axvec(4),axvec(5),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(2),axvec(4),axvec(5),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(1),axvec(3),axvec(6),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(2),axvec(3),axvec(6),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(1),axvec(4),axvec(6),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot3(axvec(2),axvec(4),axvec(6),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

Pstart1=[0;axvec(3);0];
Pend1=[0;axvec(4);0];
Pstart2=[0;0;axvec(5)];
Pend2=[0;0;axvec(6)];
Pstart3=[axvec(1);0;0];
Pend3=[axvec(2);0;0];

plotArrow3(Pstart1,Pend1,lenArr,radi,liwi);
plotArrow3(Pstart2,Pend2,lenArr,radi,liwi);
plotArrow3(Pstart3,Pend3,lenArr,radi,liwi);

text('Interpreter','latex','String','$y$','Position',[Pend1(1),Pend1(2)+0.125*spa,Pend1(3)],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$z$','Position',[Pend2(1),Pend2(2),Pend2(3)-0.125*spa],'HorizontalAlignment','center','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$x$','Position',[Pend3(1),Pend3(2)-0.25*spa,Pend3(3)],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


Nangl=700;
nr=20;
angl=linspace(-pi,pi,Nangl);

xvec=cos(angl);
yvec=sin(angl);
zvec=bound(angl);

plot3(xvec,yvec,zvec,'k-')


axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);


numTerms=40;

[th,rad] = meshgrid(linspace(-pi,pi,Nangl),linspace(0,1,nr));
[X,Y] = pol2cart(th,rad);

sN=0.5*ones(nr,Nangl);

hSn=surf(X,Y,sN,'EdgeColor','none','FaceAlpha',0.75);

hLc=zeros(1,nr);

for j=1:nr
    hLc(j)=plot3(X(j,:),Y(j,:),sN(j,:),'k-');
end

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose','-opengl',printResolution,[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

delete(hSn)
delete(hLc)

n=1;
coefPositive=true;

for i=2:numTerms
    
    if coefPositive
        sN=sN+(2/(pi*n))*cos(n*th).*rad.^n;
    else
        sN=sN-(2/(pi*n))*cos(n*th).*rad.^n;
    end
    
    hSn=surf(X,Y,sN,'EdgeColor','none','FaceAlpha',0.75);
    
    for j=1:nr
        hLc(j)=plot3(X(j,:),Y(j,:),sN(j,:),'k-');
    end
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose','-opengl',printResolution,[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hSn)
    delete(hLc)
    
    n=n+2;
    coefPositive=not(coefPositive);
    
    frmps=frmps+0.25;
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


function z=bound(theta)

z=zeros(size(theta));

for i=1:length(theta)
    
    if theta(i)<-pi/2
        
    elseif theta(i)<pi/2
        z(i)=1;
    end
    
end


function h=plotArrow3(Pstart,Pend,arrl,radi,liwi,clr)

if nargin<6
    clr=[0,0,0];
end

dir=Pend-Pstart;
dir=dir/norm(dir);

dirp=radi*null(dir');

h=[0,0];

numN=50;

angle=linspace(0,2*pi,numN);

patchPoints=zeros(3,numN+1);
patchPoints(:,1:numN)=dirp(:,1)*cos(angle)+dirp(:,2)*sin(angle);
patchPoints(:,1+numN)=arrl*dir;

trsl=Pend-arrl*dir;

patchPoints(1,:)=patchPoints(1,:)+trsl(1);
patchPoints(2,:)=patchPoints(2,:)+trsl(2);
patchPoints(3,:)=patchPoints(3,:)+trsl(3);

TRI=[1:(numN-1);2:numN;(numN+1)*ones(1,numN-1)]';

h(1)=plot3([Pstart(1),Pend(1)-0.5*arrl*dir(1)],[Pstart(2),Pend(2)-0.5*arrl*dir(2)],[Pstart(3),Pend(3)-0.5*arrl*dir(3)],'-','LineWidth',liwi,'Color',clr);
h(2)=patch('faces',TRI,'vertices',patchPoints','FaceColor',clr,'FaceAlpha',1,'EdgeColor','none');

function exampleBisection()

arrl=0.15;
arrw=arrl/5;
arrScal=0.85;

spa=0.075;

fosi=20;
font='Times';

myLW=1.1;

xyticksL=0.05;

axls=[-3 3 -1 3];
axvec=1.02*axls;

sca=100;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis image
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis
plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$x$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


pntsi=6;
numfvals=125;

xstart=axls(1);
xend=axls(2);

xvals=linspace(xstart,xend,numfvals);

myFunc=@(x)0.08*(x+4).*(x+1).*(x-1.18);

plotInAxis(axls,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
plotInAxis(axls,xvals,myFunc(xvals),'k-','LineWidth',myLW);

xFuncText=2.2;
text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)],'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=3.5in';
includegraphicsOptions='';
figureDirectory='exBisection';
figcounter=0;
filename='exBisection';
frmps=1;

mkdir(figureDirectory);

iters=4;

atmp=0.25;
btmp=2.5;

spa2=0.15;

hat=plot([atmp,atmp],[-xyticksL,0],'k-','LineWidth',myLW);

hatl0=text('Interpreter','latex','String','$a$','Position',[atmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hatl1=text('Interpreter','latex','String','$a$','Position',[atmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hatl=text('Interpreter','latex','String','$a$','Position',[atmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


hbt=plot([btmp,btmp],[-xyticksL,0],'k-','LineWidth',myLW);

hbtl0=text('Interpreter','latex','String','$b$','Position',[btmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hbtl1=text('Interpreter','latex','String','$b$','Position',[btmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hbtl=text('Interpreter','latex','String','$b$','Position',[btmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);


hapn=plot(atmp,myFunc(atmp),'bo','MarkerSize',pntsi);
hbpn=plot(btmp,myFunc(btmp),'ro','MarkerSize',pntsi);


set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

for i=0:iters
    
    ctmp=0.5*(atmp+btmp);
    fctmp=myFunc(ctmp);
    
    hct0=plot([ctmp,ctmp],[-xyticksL,0],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
    hct=plot([ctmp,ctmp],[-xyticksL,0],'k-','LineWidth',myLW);
    
    hctl0=text('Interpreter','latex','String','$c$','Position',[ctmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
    hctl1=text('Interpreter','latex','String','$c$','Position',[ctmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
    hctl=text('Interpreter','latex','String','$c$','Position',[ctmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

    if fctmp>0
        
        hcpn=plot(ctmp,fctmp,'ro','MarkerSize',pntsi);
        btmp=ctmp;
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
        delete(hbt)
        delete(hbtl0)
        delete(hbtl1)
        delete(hbtl)
        delete(hbpn)
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
        delete(hct0)
        delete(hct)
        delete(hctl0)
        delete(hctl1)
        delete(hctl)
        delete(hcpn)
        
        hbt=plot([btmp,btmp],[-xyticksL,0],'k-','LineWidth',myLW);
        
        hbtl0=text('Interpreter','latex','String','$b$','Position',[btmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
        hbtl1=text('Interpreter','latex','String','$b$','Position',[btmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
        hbtl=text('Interpreter','latex','String','$b$','Position',[btmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
        hbpn=plot(btmp,myFunc(btmp),'ro','MarkerSize',pntsi);
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
    else
        
        hcpn=plot(ctmp,fctmp,'bo','MarkerSize',pntsi);
        atmp=ctmp;
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
        delete(hat)
        delete(hatl0)
        delete(hatl1)
        delete(hatl)
        delete(hapn)
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
        delete(hct0)
        delete(hct)
        delete(hctl0)
        delete(hctl1)
        delete(hctl)
        delete(hcpn)
        
        hat=plot([atmp,atmp],[-xyticksL,0],'k-','LineWidth',myLW);
        
        hatl0=text('Interpreter','latex','String','$a$','Position',[atmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
        hatl1=text('Interpreter','latex','String','$a$','Position',[atmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
        hatl=text('Interpreter','latex','String','$a$','Position',[atmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
        hapn=plot(atmp,myFunc(atmp),'bo','MarkerSize',pntsi);
        
        set(gcf,'PaperPositionMode','auto')
        
        figcounter=figcounter+1;
        print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
        AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
        
    end
    
end

ctmp=0.5*(atmp+btmp);
fctmp=myFunc(ctmp);

hct0=plot([ctmp,ctmp],[-xyticksL,0],'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hct=plot([ctmp,ctmp],[-xyticksL,0],'k-','LineWidth',myLW);

hctl0=text('Interpreter','latex','String','$c$','Position',[ctmp-arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hctl1=text('Interpreter','latex','String','$c$','Position',[ctmp+arrw/4,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color',[1 1 1]*0.99999);
hctl=text('Interpreter','latex','String','$c$','Position',[ctmp,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);


hcpn=plot(ctmp,fctmp,'go','MarkerSize',pntsi);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

delete(hat)
delete(hatl0)
delete(hatl1)
delete(hatl)
delete(hapn)
delete(hbt)
delete(hbtl0)
delete(hbtl1)
delete(hbtl)
delete(hbpn)


set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


function h=plotInAxis(axls,xvals,yvals,varargin)

isInside=and(and(xvals>axls(1),xvals<axls(2)),and(yvals>axls(3),yvals<axls(4)));

h=plot(xvals(isInside),yvals(isInside),varargin{:});

function h=plotArrow(astart,aend,arrl,arrw,sc,lw,clr)

if nargin<7
    clr=[0,0,0];
end

h=[0,0];

dirc=aend-astart;

lgt=norm(dirc);

dirc=dirc/lgt;

xcords=lgt+[-sc*arrl -sc*arrl -arrl 0 -arrl -sc*arrl];
ycords=[0 arrw*1e-3 arrw 0 -arrw -arrw*1e-3];

aline=[astart astart+(lgt-0.5*sc*arrl)*dirc];

h(1)=plot(aline(1,:),aline(2,:),'-','LineWidth',lw,'Color',clr);

xcords2=dirc(1)*xcords-dirc(2)*ycords+astart(1);
ycords2=dirc(2)*xcords+dirc(1)*ycords+astart(2);

h(2)=patch(xcords2,ycords2,'k');

set(h(2),'FaceColor',clr,'EdgeColor',clr);

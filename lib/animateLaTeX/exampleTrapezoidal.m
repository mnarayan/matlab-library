function exampleTrapezoidal()

arrl=0.11;
arrw=arrl/5;
arrScal=0.85;

spa=0.04;
spa2=0.1;

fosi=16;
font='Times';

myLW=0.8;

xyticksL=0.05;

axls=[-0.25 3.85 -1.5 1.85];
axvec=1.02*axls;

precStr='%6.4f';
ylRieText=-1.35;

sca=130;

figure('Position',[50 50 sca*(axvec(2)-axvec(1)) sca*(axvec(4)-axvec(3))]),hold on;
set(gca,'XTick',[],'YTick',[]);
set(gca,'Position',[0 0 1 1]);
axis(axvec);
axis equal
axis off;

plot(axvec(1),axvec(3),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);
plot(axvec(2),axvec(4),'.','Color',0.9999*[1 1 1],'MarkerSize',0.1);

% Plot the coordinate axis

hxax=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
plotArrow([0;axls(3)],[0;axls(4)],arrl,arrw,arrScal,myLW);
text('Interpreter','latex','String','$x$','Position',[axls(2)-arrl,arrw+spa],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
text('Interpreter','latex','String','$y$','Position',[arrw+spa,axls(4)-arrl],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


numfvals=280;

xstart=axls(1);
xend=axls(2);

xvals=linspace(xstart,xend,numfvals);

myFunc=@(x)(x-0.45).*(x-1.87).*(x-3.11);
myFuncNeg=@(x)-myFunc(x);

a=0.2;
b=3.45;


xFuncText=fminbnd(myFuncNeg,0.6,1.5);

hfunc0=plotInAxis(axls,xvals,myFunc(xvals),'-','LineWidth',2.2*myLW,'Color',[1 1 1]*0.99999);
hfunc=plotInAxis(axls,xvals,myFunc(xvals),'k-','LineWidth',myLW);

hfuncText=text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)+spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

hat=plot([a,a],[-xyticksL,0],'k-','LineWidth',myLW);
ha=text('Interpreter','latex','String','$a$','Position',[a,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');

hbt=plot([b,b],[-xyticksL,0],'k-','LineWidth',myLW);
hb=text('Interpreter','latex','String','$b$','Position',[b,-spa-xyticksL-spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');


% Do the animation

AniLaTeX=[];
animategraphicsOptions='controls,width=3.5in';
includegraphicsOptions='';
figureDirectory='exTrapezoidalRule';
figcounter=0;
filename='exTrapezoidalRule';
frmps=1.5;

mkdir(figureDirectory);

axis(axvec);

set(gcf,'PaperPositionMode','auto')

figcounter=figcounter+1;
print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);


delete(hfunc0);
delete(hfunc);
delete(hfuncText);
delete(hxax);
delete(hat);
delete(ha);
delete(hbt);
delete(hb);


ba=b-a;

imax=30;

for i=1:imax
    
    deltaX=ba/i;
    hrectL=zeros(1,i);
    
    lRieVal=0;
    for j=1:i
        xvalstmp=[a+(j-1)*deltaX,a+j*deltaX];
        fvalstmp=myFunc(xvalstmp);
        tmpVal=sum(fvalstmp)*deltaX;
        
        if tmpVal>0
            clr=[1,0.666,0.666];
        else
            clr=[0.666,0.666,1];
        end
        
        lRieVal=lRieVal+tmpVal;
        
        hrectL(j)=patch([a+(j-0.5)*deltaX,a+(j-1)*deltaX,a+(j-1)*deltaX,a+j*deltaX,a+j*deltaX,a+(j-0.5)*deltaX],[0,0,fvalstmp(1),fvalstmp(2),0,0],'k');
        set(hrectL(j),'FaceColor',clr,'EdgeColor',0.666666667*clr,'LineWidth',0.1);
    end
    lRieVal=lRieVal/2;
    
    hxax=plotArrow([axls(1);0],[axls(2);0],arrl,arrw,arrScal,myLW);
    hfunc=plotInAxis(axls,xvals,myFunc(xvals),'k-','LineWidth',myLW);
    hfuncText=text('Interpreter','latex','String','$y=f(x)$','Position',[xFuncText,myFunc(xFuncText)+spa2],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
    hlTrapzf=text('Interpreter','latex','String','$T(f,$','Position',[xFuncText+0.34,ylRieText],'HorizontalAlignment','right','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
    hlTrapzP=text('Interpreter','latex','String',['$P_{',num2str(i),'}$'],'Position',[xFuncText+0.45,ylRieText],'HorizontalAlignment','center','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
    hlTrapz=text('Interpreter','latex','String',['$)=',num2str(lRieVal,precStr),'$'],'Position',[xFuncText+0.55,ylRieText],'HorizontalAlignment','left','VerticalAlignment','baseline','FontSize',fosi,'FontName',font,'Color','k');
    
    axis(axvec);
    
    set(gcf,'PaperPositionMode','auto')
    
    figcounter=figcounter+1;
    print(gcf,'-depsc2','-loose',[figureDirectory,'/',filename,'No',num2str(figcounter),'.eps']);
    AniLaTeX=animateinlineLaTeX(AniLaTeX,[filename,'No',num2str(figcounter),'.eps'],figureDirectory,includegraphicsOptions,frmps);
    
    delete(hfunc);
    delete(hfuncText);
    delete(hxax);
    delete(hlTrapzf);
    delete(hlTrapzP);
    delete(hlTrapz);
    delete(hrectL);
    
end

animateinlineLaTeX(AniLaTeX,[figureDirectory,'/',filename],animategraphicsOptions);
close;


function h=plotInAxis(axls,xvals,yvals,varargin)

isInside=and(and(xvals>axls(1),xvals<axls(2)),and(yvals>axls(3),yvals<axls(4)));

h=plot(xvals(isInside),yvals(isInside),varargin{:});

function h=plotArrow(astart,aend,arrl,arrw,sc,lw,clr)

if nargin<7
    clr=[0,0,0];
end

h=[0,0];

dirc=aend-astart;

lgt=norm(dirc);

dirc=dirc/lgt;

xcords=lgt+[-sc*arrl -sc*arrl -arrl 0 -arrl -sc*arrl];
ycords=[0 arrw*1e-3 arrw 0 -arrw -arrw*1e-3];

aline=[astart astart+(lgt-0.5*sc*arrl)*dirc];

h(1)=plot(aline(1,:),aline(2,:),'-','LineWidth',lw,'Color',clr);

xcords2=dirc(1)*xcords-dirc(2)*ycords+astart(1);
ycords2=dirc(2)*xcords+dirc(1)*ycords+astart(2);

h(2)=patch(xcords2,ycords2,'k');

set(h(2),'FaceColor',clr,'EdgeColor',clr);

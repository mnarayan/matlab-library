# Advanced Software Development
git submodule update --remote lib/mopi
git submodule update --remote lib/matlabdoc
git submodule update --remote lib/ArgUtils
git submodule update --remote lib/logging4matlab
git submodule update --remote lib/MatlabProgressBar
git submodule update --remote lib/matlab-ezparse
git submodule update --remote lib/MATLAB-ParseArgs

### Pipeline, Job management
git submodule update --remote lib/psom
git submodule update --remote lib/matlab-batch
git submodule update --remote lib/CachePureFunction
#git submodule update --remote lib/ForEach    #Not a gitrepo
#git submodule update --remote lib/DataHash
#git submodule update --remote lib/MapN
#git submodule update --remote lib/matlab_makefile

### Testing
git submodule update --remote test/MOxUnit
git submodule update --remote test/matlab-xunit

# Plotting Tools
git submodule update --remote lib/BrewerMap
git submodule update --remote lib/boundedline
git submodule update --remote lib/legendflex
git submodule update --remote lib/plotgrid
git submodule update --remote lib/gramm
git submodule update --remote lib/columnlegend
git submodule update --remote lib/matlab-plotly
git fetch superbar master
git subtree pull --prefix lib/superbar superbar --squash
git fetch radarplot master
git subtree pull --prefix lib/radarplot radarplot --squash

# Export Tools
git submodule update --remote lib/latexTable
git submodule update --remote lib/matlab-json
git submodule update --remote lib/export_fig
git submodule update --remote lib/saveFigure
git submodule update --remote lib/matlab2tikz

# Convenience
git submodule update --remote lib/MATLAB-git